'use strict';

let gulp = require('gulp'),
    browserSync = require('browser-sync'),
    del = require('del'),
    autoPrefixer = require('gulp-autoprefixer'),
    cleanCss = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    imageMin = require('gulp-imagemin'),
    notify = require('gulp-notify'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    rsync = require('gulp-rsync'),
    rigger = require('gulp-rigger'),
    sourceMaps = require('gulp-sourcemaps'),
    clearCache = require('gulp-cache');

let syntax = 'sass',
    gulpVersion = 4;

let path = {
    build: {
        html: 'app/build/',
        js: 'app/build/js/',
        css: 'app/build/css/',
        images: 'app/build/images/',
        fonts: 'app/build/fonts/',
    },
    src: {
        html: 'app/*.html',
        css: ['app/' + syntax + '/**/*.' + syntax + '', 'app/css/style.css'],
        images: 'app/images/**/*.*',
        fonts: 'app/fonts/**/*.*'
    },
    watch: {
        html: ['app/*.html', 'app/components/**/*.html'],
        js: ['libs/**/*.js', 'app/js/main.js'],
        css: ['app/' + syntax + '/**/*.' + syntax + '', 'app/css/style.css'],
        images: 'app/images/**/*.*',
        fonts: 'app/fonts/**/*.*'
    }
};


// Local server configs
gulp.task('browser-sync', () => {
    browserSync({
        server: {
            baseDir: 'app/build',
            routes: {
                "/example": "app/build/example.html"
            }
        },
        notify: false,
        // open: false,
        // online: false,
        // tunnel: true, tunnel: "projectName", // Demonstration page: http://projectname.localtunnel.me
    });
});

// Sass|Scss Styles
gulp.task('styles:build', () => {
    return gulp.src(path.src.css)
        .pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
        .pipe(concat('main.min.css'))
        .pipe(autoPrefixer(['last 15 versions']))
        .pipe(cleanCss({ level: { 1: { specialComments: 0 } } })) // Disable comments on code
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream());
});

// JavaScripts
gulp.task('scripts:build', () => {
    return gulp.src([
            'app/libs/jquery/dist/jquery.min.js',
            'app/js/owl.carousel.min.js',
            'app/libs/fancybox/dist/jquery.fancybox.min.js',
            'app/js/main.js', // Always last
        ]).pipe(concat('scripts.min.js'))
        .pipe(sourceMaps.init())
        .pipe(uglify())
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream());
});

// HTML
gulp.task('html:build', () => {
    return gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream());
});

// Images
gulp.task('images:build', () => {
    gulp.src(path.src.images)
        .pipe(imageMin())
        .pipe(gulp.dest(path.build.images))
        .pipe(browserSync.reload({ stream: true }));
});

// Fonts
gulp.task('fonts:build', () => {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(browserSync.reload({ stream: true }));
});

// Other files (example: .htaccess, sitemap ...)
gulp.task('other:build', () => {
    gulp.src([
            'app/.htaccess'
        ]).pipe(gulp.dest('app/build'))
        .pipe(browserSync.reload({ stream: true }));
});

// Remove and clear
gulp.task('removeBuild', () => { return del.sync('app/build'); });
gulp.task('clearCache', () => { return clearCache.clearAll(); });

// Deploy
gulp.task('rsync', () => {
    return gulp.src('app/build/**')
        .pipe(rsync({
            root: 'app/build/',
            hostname: 'username@site.com',
            destination: 'site/public_html/',
            // include: ['*.htaccess'],
            exclude: ['**/Thumbs.db', '**/*.DS_Store'],
            recursive: true,
            archive: true,
            silent: false,
            compress: true
        }));
});

// Gulp
if (gulpVersion === 4) {

    gulp.task('watch', () => {
        gulp.watch(path.watch.css, gulp.parallel('styles:build'));
        gulp.watch(path.watch.js, gulp.parallel('scripts:build'));
        gulp.watch(path.watch.html, gulp.parallel('html:build'));
        gulp.watch(path.watch.images, gulp.parallel('images:build'));
        gulp.watch(path.watch.fonts, gulp.parallel('fonts:build'));
        gulp.watch(['app/.htaccess'], gulp.parallel('other:build'));
    });

    gulp.task('default', gulp.parallel(
        'removeBuild',
        'html:build',
        'scripts:build',
        'styles:build',
        'fonts:build',
        'images:build',
        'other:build',
        'browser-sync',
        'watch'
    ));
}