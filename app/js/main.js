;$(function() {

    const skillList = {
        html: {
            name: "Html",
            description: "Отлично ориентируюсь в вёрстке, способен быстро и качественно составить скелет сайта, очень стараюсь содержать код в чистоте и уюте, разделять все по компонентам.",
            levelPercent: 90
        },
        css: {
            name: "Css, Scss, Sass, Less",
            description: "Хорошо владею навыком написания стилей для сайта, достаточно легко могу определить проблемные места и найти решение. В начале карьеры веб-разработчиком часто использовал less, сейчас он остался скорее как опыт. На данный момент пишу либо на чистом css, либо на sass/scss (данный сайт написан на sass). Стараюсь соблюдать БЭМ методологию, где это возможно.",
            levelPercent: 85,
            className: "css"
        },
        bootstrap: {
            name: "Bootstrap",
            description: "Имел частый опыт верстки bootstrap страниц (особенно на некоторых фриланс задачах). На данный момент на работе и при верстке своих сайтов данную технологию не использую, но в учебных проектах она всегда присутствует."
        },
        php: {
            name: "Php",
            description: "Мой главный рабочий инструмент! Php нравится мне тем, что это обычный язык программирования, который без всяких 'выкрутасов' и 'наворотов' дает разработчику то, что ему нужно. В плохом коде всегда нужно задавать вопросы людям, а не языку программирования, поэтому, как бы php не хейтили - для меня он прекрасен! Слабые стороны: работа с файлами, настройка на сервере (пока что мало опыта с этим).",
            levelPercent: 85
        },
        laravel: {
            name: "Laravel",
            description: "В этот фреймворк я, честно говоря, влюбился. Постоянно задумываюсь, с какой любовью его создавали и поддерживают сейчас. Очень приятен в работе, очень продуманный, дает огромный выбор в реализации и при этом большую свободу действий. Также у них прекрасная документация, которая поможет в 90% случаев. Пока что я только изучаю данный фреймворк (на данный момент имею 2 проекта на нем, один из них - <a href='https://moydameti.ru/' target='_blank'>Мой да мети</a>).",
            levelPercent: 45
        },
        bitrix: {
            name: "Bitrix",
            description: "Собственно, сайт eldorado.ru - это и есть битрикс. Имею хорошие навыки по работе с данным фреймворком. Написал приличное кол-во компонентов, да и в целом прочитал и написал очень много кода с его помощью. Хоть bitrix и не очень любят, я позитивно отношусь к данному фреймворку. Я не очень силен в ядре bitrix или множестве его методов, но для качественного и быстрого решения задач по сопровождению сайта мне моих знаний хватает. Все остальное легко можно найти, т.к. база знаний по этому фреймворку очень большая.",
            levelPercent: 70
        },
        symfony: {
            name: "Symfony",
            description: "Имею очень большое желание изучить данный фреймворк, поэтому и указал его тут. Он точно входит в мои планы на ближайший год!",
            levelPercent: 5
        },
        js: {
            name: "Js",
            description: "На первой работе я очень активно изучал js, но мне всегда плоховато давались 'хитрости' данного языка, а их много. Однако, по работе, да и в целом со временем, многие вещи изучил на практике и даже часто стараюсь писать именно нативный js код (использую vanilla.js, так сказать). Слабых сторон, конечно, осталось еще прилично, но для меня это лишь вопрос времени и опыта. Также владею jQuery.",
            levelPercent: 70
        },
        react: {
            name: "React",
            description: "По работе я также познакомился с React. Прошел курсы по React + Redux, изучил множество материалов и видео. Сайт <a href='https://moydameti.ru/'>Мой да мети</a>, кстати, написан мной на React + Redux + typescript (в качестве backend Laravel). Пока еще не могу сказать, что пишу красивый и качественный код с точки зрения javascript'а, но я очень стараюсь и учусь.",
            levelPercent: 60
        },
        redux: {
            name: "Redux",
            description: "По работе познакомился с данной библиотекой, написал с ее использованием сайт <a href='https://moydameti.ru/'>Мой да мети</a>. Опыт использования не очень большой, но понимание логики работы есть.",
            levelPercent: 50
        },
        typescript: {
            name: "Typescript",
            description: "По работе познакомился с данной библиотекой, написал с ее использованием сайт <a href='https://moydameti.ru/'>Мой да мети</a>. Считаю это очень крутым и полезным инструментом, намерен постоянно использовать его в будущем, особенно при разработке React приложений.",
            levelPercent: 60
        },
        webpack: {
            name: "Webpack, Gulp",
            description: "Имею опыт работы с данными технологиями (например, я даже написал свой <a href='https://bitbucket.org/vita1ya/picker/' target='_blank'>сборщик проектов</a> с использованием gulp, с помощью которого, кстати, написан данный сайт (и нашёл в сборщике пару багов :)).",
            levelPercent: 35,
            className: "webpack"
        },
        mysql: {
            name: "MySql",
            description: "Моя рабочая СУБД. На данный момент использую её во всех проектах и на работе. Имею опыт построения структуры БД, неплохо пишу и ориентируюсь в запросах.",
            levelPercent: 80
        },
        postgresql: {
            name: "PostgreSql",
            description: "Имел небольшой опыт работы с данной СУБД. Т.к. это SQL СУБД, с радостью готов с ней работать, однако, никаких её тонкостей и отличий от MySql не знаю (слышал только, что в некоторых вещах postgresql быстрее).",
        },
        mongodb: {
            name: "MongoDB",
            description: "Имел опыт работы с данной СУБД на первой работе (через библиотеку mongoose), очень понравилась, но больше не доводилось ее нигде использовать.",
        },
        redis: {
            name: "Redis",
            description: "Мне кажется, что redis сейчас используют практически везде, т.к. это очень удобная СУБД для хранения данных в формате ключ - значение. На работе и в личных проектах постоянно использую redis.",
        },
        git: {
            name: "Git",
            description: "Хорошо владею командами, работаю как в консоли, так и через среду программирования (у меня это, кстати, phpstorm). Лично использую Bitbucket и github. На работе - gitlab.",
            levelPercent: 80
        },
        linux: {
            name: "Linux",
            description: "Очень люблю эту ОС, на первой работе на ноутбук ставил себе 16-ю Ubuntu и целый код с радостью ею пользовался. На данный момент в основном работаю на Windows, но имею Mac, на котором есть любимая командная строка! В основном, навыки базовые, но пару лет назад проходил курсы на stepik, где близко знакомился с многими вещами. <br> P.s. я даже смогу выйти из vim :)",
            levelPercent: 70
        },
        jira: {
            name: "Jira",
            description: "Рабочий инструмент, умею настраивать и писать фильтры, хорошо ориентируюсь в программе. С точки зрения сотрудника сопровождения - инструмент незаменимый, особенно, если правильно настроить все процессы (но этим я не занимался).",
        },
        google: {
            name: "Google",
            description: "Прекрасно владею механизмом поиска, 10 раз погугли, 1 раз спроси у коллег. За вопросами всегда в первую очередь лезу в google, активно пользуюсь всеми возможностями глобальной сети интернет. Лучшее решение можно написать за 3 дня, ну или найти за 3 минуты :)",
            levelPercent: 99
        }
    };

    const skillsTree = [
        {
            title: "Html + Css",
            elements: [
                skillList.html,
                skillList.css,
                skillList.bootstrap
            ]
        },
        {
            title: "Php",
            elements: [
                skillList.php,
                skillList.laravel,
                skillList.bitrix,
                skillList.symfony,
            ]
        },
        {
            title: "Js",
            elements: [
                skillList.js,
                skillList.jquery,
                skillList.react,
                skillList.redux,
                skillList.typescript,
                skillList.webpack,
            ]
        },
        {
            title: "Database",
            elements: [
                skillList.mysql,
                skillList.postgresql,
                skillList.mongodb,
                skillList.redis,
            ]
        },
        {
            title: "Other",
            elements: [
                skillList.git,
                skillList.linux,
                skillList.jira,
                skillList.google,
            ]
        }
    ];

    let filterListSelector = ".skills__filter-list",
        filterElementSelector = '.skills__filter',
        activeFilterElementSelector = '.skills__filter.active',
        skillsListSelector = '.skills__list',
        skillSelector = '.skills__element',
        timelineBlocks = $('.work-card'),
        topMenu = $('.top-menu'),
        inputNameSelector = $("#name"),
        inputEmailSelector = $("#email"),
        inputMsgSelector = $("#message");

    $(document).ready(function () {
        fillFilters();
        showSkills();

        hideTimelineBlocks(timelineBlocks);

        initCarousel();

        addMenuOnClick();

        changeMenuPosition();

        changeMenuActiveElement();

        setYear();

        year18Click();

        sendContactMe();

        initMobileEvents();
    });

    $(window).on('scroll', function(){
        !window.requestAnimationFrame
            ? setTimeout(() => showTimelineBlocks(timelineBlocks), 100)
            : window.requestAnimationFrame(() => showTimelineBlocks(timelineBlocks));

        changeMenuActiveElement();

        changeMenuPosition();
    });
    
    function fillFilters() {
        if (isMobile()) {
            $(filterListSelector).append("<li class='skills__filter'>Все</li>");
        } else {
            $(filterListSelector).append("<li class='skills__filter active'>Все</li>");
        }

        for (const skills of skillsTree) {
            if (isMobile() && skills.title === 'Php') {
                $(filterListSelector).append(`<li class='skills__filter active'>${skills.title}</li>`);
            } else {
                $(filterListSelector).append(`<li class='skills__filter'>${skills.title}</li>`);
            }
        }

        addFilterElementOnClick();
    }

    function addFilterElementOnClick() {
        $(filterElementSelector).on("click", function () {
            if ($(this).hasClass('active')) return;

            $(activeFilterElementSelector).removeClass('active');
            $(this).addClass('active');

            showSkills();
        });
    }
    
    function showSkills() {
        $(skillsListSelector).html("");

        for (const skills of skillsTree) {
            if (skills.title === $(activeFilterElementSelector).text() || $(activeFilterElementSelector).text() === 'Все') {
                createSkills(skills);
            }
        }

        if (!isMobile()) {
            const firstSkill = $(skillSelector).first();
            firstSkill.addClass('active');

            showSkill(firstSkill.data('code'));
        }

        addSkillOnClick();
    }

    function createSkills(skills) {
        for (const skill of skills.elements) {
            $(skillsListSelector).append(createSkillElement(skill));
        }
    }

    function createSkillElement(element) {
        if (!element) return "";

        const lowerName = element.className || element.name.toLowerCase();
        return `<li class='skills__element' 
                    data-code="${lowerName}"><span class="skills__element-name">${element.name}</span></li>`;
    }

    function showSkill(code) {
        const skill = skillList[code];

        if (!isMobile()) {
            const lowerName = skill.className || skill.name.toLowerCase();
            $('.skill__name').attr("data-code", lowerName).text(skill.name);
        }

        if (skill.levelPercent) {
            $('.skill__percent-number').text(`${skill.levelPercent}%`);
            $('.skill__percent-block span').css({
                "width": `${skill.levelPercent}%`,
                "backgroundColor": getBackgroundColorByPercent(skill.levelPercent),
                "borderRight": `1px solid ${getBackgroundColorByPercent(skill.levelPercent)}`
            });
            $(".skill__percent").show();
        } else {
            $(".skill__percent").hide();
        }

        $('.skill__description').html(skill.description);

        if (isMobile()) {
            $(".skill").appendTo(`.skills__element[data-code=${code}]`);
        }
    }

    function addSkillOnClick() {
        $(skillSelector).on("click", function () {
            if ($(this).hasClass('active')) {
                if (isMobile()) {
                    $(this).find(".skill").slideToggle(100);
                }
                $(this).removeClass('active');
                return;
            }

            const activeSkill = $(`${skillSelector}.active`);
            if (isMobile()) {
                //activeSkill.find(".skill").slideUp();
            }
            activeSkill.removeClass('active');
            $(this).addClass('active');

            showSkill($(this).data('code'));

            if (isMobile()) {
                $(this).find(".skill").slideDown(200);
            }
        });
    }

    function getBackgroundColorByPercent(percent) {
        if (percent < 40) {
            return "#C10230";
        }

        if (percent < 70) {
            return "#fedb65";
        }

        return "#76bc21";
    }

    function hideTimelineBlocks(blocks) {
        blocks.each(function() {
            if ($(this).offset().top > getWindowScrollOffset(0.8)) {
                $(this).addClass('is-hidden');
            }
        });
    }

    function showTimelineBlocks(blocks) {
        blocks.each(function() {
            if ($(this).offset().top <= getWindowScrollOffset(0.8) && $(this).hasClass('is-hidden')) {
                $(this).removeClass('is-hidden').addClass('bounce-in');
            }
        });
    }

    function getWindowScrollOffset(offset) {
        return $(window).scrollTop() + $(window).height() * offset;
    }

    function initCarousel() {
        $('.achievements .owl-carousel').owlCarousel({
            items: isMobile() ? 2 : 3,
            loop: true,
            margin: 20,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true
        });

        $('.books .owl-carousel').owlCarousel({
            items: isMobile() ? 3 : 5,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true
        });
    }

    function addMenuOnClick() {
        $(".menu__element").on("click", function (e) {
            if ($(this).hasClass('active')) return;

            e.preventDefault();

            $(".menu__element.active").removeClass('active');
            $(this).addClass('active');

            const href = $(this).children().attr('href');
            $('html, body').animate({
                scrollTop: $(href).offset().top - $(".top-menu").height()
            }, {
                duration: 600,
                easing: "linear"
            });

            return false;
        });

        $(".top-menu__logo").on("click", function (e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, {
                duration: 600,
                easing: "linear"
            });

            return false;
        });
    }

    function changeMenuActiveElement() {
        $(".scroll-element").each(function() {
            if ($(this).offset().top <= getWindowScrollOffset(0.2)) {
                $(".menu__element.active").removeClass('active');

                const id = `#${$(this).attr("id")}`;
                $(`a[href="${id}"]`).parent().addClass('active');
            }
        });
    }

    function changeMenuPosition() {
        if (isMobile()) return;

        if($(this).scrollTop() > 140) {
            if (topMenu.hasClass('fixed')) return;

            topMenu.hide().addClass('fixed').fadeIn(500);
        } else if ($(this).scrollTop() === 0) {
            if (topMenu.hasClass('fixed')) {
                topMenu.removeClass('fixed');
            }
        }
    }

    function setYear() {
        $('#year').text(new Date().getFullYear());

        $('.experience').text(new Date().getFullYear() - 2017);
    }

    function year18Click() {
        $('.interest__years18').on("click", function () {
            $(this).fadeOut(500);
            setTimeout(() => $(this).detach(), 3000);
        });
    }

    function sendContactMe() {
        $("#contact-me__form").submit(function (e) {
            e.preventDefault();

            $(".error").removeClass("error");
            $(".error-message").detach();

            const name = inputNameSelector.val().trim(),
                email = inputEmailSelector.val().trim(),
                msg = inputMsgSelector.val().trim();

            let error = false;

            if (
                window.localStorage
                && window.localStorage.getItem('email')
                && window.localStorage.getItem('email').indexOf(email) !== -1
            ) {
                error = true;
                inputEmailSelector.addClass("error").after("<p class='error-message'>По данному e-mail уже отправляли сообщение. Я обязательно свяжусь с Вами!</p>");
            }

            if (!name) {
                error = true;
                inputNameSelector.addClass("error").after("<p class='error-message'>Поле необходимо заполнить</p>");
            }
            if (!email) {
                error = true;
                inputEmailSelector.addClass("error").after("<p class='error-message'>Поле необходимо заполнить</p>");
            }
            if (!msg) {
                error = true;
                inputMsgSelector.addClass("error").after("<p class='error-message'>Поле необходимо заполнить</p>");
            }

            if (error) {
                return;
            }

            $(".contact-me").append("<div class='form-blur'>Отправка...</div>");

            if (window.localStorage) {
                window.localStorage.setItem(
                    'email',
                    window.localStorage.getItem('email')
                        ? window.localStorage.getItem('email') + `, ${email}`
                        : email
                );
            }

            const message = `Имя: ${name}\nE-mail: ${email}\nСообщение: ${msg}`;

            $.ajax({
                type: 'POST',
                url: `https://api.telegram.org/bot5192528812:AAHq2aget4Cq6yKBO40mE4oVcqDDDpxaaV0/sendMessage`,
                data: {
                    chat_id: 277642153,
                    text: message,
                },
                success: function () {
                    $(".error").removeClass("error");
                    $(".error-message").detach();

                    $(".form-blur").html("Сообщение отправлено!<br>Я обязательно с Вами свяжусь!");
                },
                error: function () {
                    $(".form-blur").html("Ошибка отправки, попробуйте позже :(");
                    setTimeout(() => {
                        $(".form-blur").detach();
                    }, 5000)
                }
            });
        });
    }

    function initMobileEvents() {
        if (!isMobile()) return;

        $(".mobile-hamburg").on("click", function () {
            topMenu.show(200);
        });

        $(".close-menu").on("click", function () {
            topMenu.hide(200);
        });

        $(".work-card__project").on("click", function () {
            const parent = $(this).parent().parent();
            const findSpanHtml = $(this).find("span").html();
            const mobileProjectBlock = parent.find(".mobile-project");

            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                mobileProjectBlock.slideToggle(200);
                return;
            }

            if (mobileProjectBlock.length > 0) {
                $(".work-card__project.active").removeClass('active');
                $(this).addClass('active');

                mobileProjectBlock.html(findSpanHtml);
                if (!mobileProjectBlock.is(":visible")) {
                    mobileProjectBlock.slideToggle(200);
                }
                return;
            }

            parent.append(`<div class='mobile-project'>${findSpanHtml}</div>`);
            $(this).addClass('active');
            parent.find(".mobile-project").slideDown(200);
        });
    }

    function isMobile() {
        return $(".mobile-hamburg").is(":visible");
    }
});